CREATE DATABASE  IF NOT EXISTS `smhi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smhi`;
-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: smhi
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Data`
--

DROP TABLE IF EXISTS `Data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Data` (
  `DataId` int(11) NOT NULL AUTO_INCREMENT,
  `StationId` int(11) NOT NULL,
  `ParameterId` int(11) NOT NULL,
  `PeriodId` int(11) NOT NULL,
  `Temperature` varchar(45) NOT NULL,
  `DateValue` varchar(45) DEFAULT NULL,
  `TimeValue` varchar(45) DEFAULT NULL,
  `DateTimeValue` datetime NOT NULL,
  PRIMARY KEY (`DataId`),
  KEY `idx_Data_DateTimeValue` (`DateTimeValue`)
) ENGINE=InnoDB AUTO_INCREMENT=888985 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Periods`
--

DROP TABLE IF EXISTS `Periods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Periods` (
  `PeriodId` int(11) NOT NULL,
  `PeriodName` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PeriodId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `RunConfig`
--

DROP TABLE IF EXISTS `RunConfig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RunConfig` (
  `RunId` int(11) NOT NULL AUTO_INCREMENT,
  `StationId` int(11) NOT NULL,
  `ParameterId` int(11) NOT NULL,
  `PeriodId` varchar(45) NOT NULL,
  `Enabled` int(11) NOT NULL,
  PRIMARY KEY (`RunId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SmhiParameters`
--

DROP TABLE IF EXISTS `SmhiParameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SmhiParameters` (
  `KeyId` int(11) NOT NULL,
  `Title` varchar(45) NOT NULL,
  `Summary` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`KeyId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Stations`
--

DROP TABLE IF EXISTS `Stations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Stations` (
  `StationId` int(11) NOT NULL AUTO_INCREMENT,
  `StationName` varchar(45) NOT NULL,
  `Latitud` double DEFAULT NULL,
  `Longitud` decimal(10,0) DEFAULT NULL,
  `Height` int(11) DEFAULT NULL,
  `FromDateTime` varchar(45) DEFAULT NULL,
  `ToDateTime` varchar(45) DEFAULT NULL,
  `Active` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`StationId`)
) ENGINE=InnoDB AUTO_INCREMENT=192841 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-01 16:29:12
